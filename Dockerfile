#
# Docker PHP
#

#
# Add build arg to change the base php image
#
ARG base_image="7.1-fpm"

#
# Define the base image
#
FROM php:${base_image}

#
# The php extensions to be installed in this image
#
ARG extensions=""

#
# Install the build decencies
#
RUN apt-get update
RUN apt-get install -y sudo tar curl lsb-release

#
# Export the php extensions dir so we know where the source are
#
ENV PHP_EXTENSIONS_DIR=/usr/src/php/ext

#
# Install phpenv-practically standalone
#
RUN curl -o /tmp/pphp https://git.zportal.co.uk/incubator/phpenv-practically/-/archive/master/phpenv-practically-master.tar.gz \
    && tar -xzf /tmp/pphp -C /tmp \
    && mkdir -p /root/.pphp \
    && cp -R /tmp/phpenv-practically*/* /root/.pphp \
    && docker-php-source extract

#
# Install php extensions
#
RUN if [ ! -z "${extensions}" ]; then /root/.pphp/bin/phpenv-ext-install ${extensions}; fi

#
# Install composer
#
RUN apt-get install -y unzip
RUN curl --show-error https://getcomposer.org/installer | php;
RUN mv composer.phar /usr/bin/composer;

#
# Clean up
#
RUN SUDO_FORCE_REMOVE=yes apt-get remove -y --purge sudo lsb-release
RUN apt-get -y autoremove && apt-get autoclean \
    && docker-php-source delete \
    && rm -rf /tmp/* \
    && rm -rf /root/.pphp \
    && rm -rf /var/lib/apt/lists/*
