REGISTRY=local/php

php-7.1: php-7.1-cli php-7.1-fpm

php-7.1-cli:
	 docker build --pull --build-arg base_image=7.1-cli --build-arg extensions="pdo pdo_mysql zip intl gd xdebug" --tag ${REGISTRY}:7.1-cli .

php-7.1-fpm:
	 docker build --pull --build-arg base_image=7.1-fpm --build-arg extensions="pdo pdo_mysql zip intl gd" --tag ${REGISTRY}:7.1-fpm .

php-7.2: php-7.2-cli php-7.2-fpm

php-7.2-cli:
	 docker build --pull --build-arg base_image=7.2-cli --build-arg extensions="pdo pdo_mysql zip intl gd xdebug" --tag ${REGISTRY}:7.2-cli .

php-7.2-fpm:
	 docker build --pull --build-arg base_image=7.2-fpm  --build-arg extensions="pdo pdo_mysql zip intl gd" --tag ${REGISTRY}:7.2-fpm .

php-7.3: php-7.3-cli php-7.3-fpm

php-7.3-cli:
	 docker build --pull --build-arg base_image=7.3-cli --build-arg extensions="pdo pdo_mysql zip intl gd xdebug" --tag ${REGISTRY}:7.3-cli .

php-7.3-fpm:
	 docker build --pull --build-arg base_image=7.3-fpm  --build-arg extensions="pdo pdo_mysql zip intl gd" --tag ${REGISTRY}:7.3-fpm .

php-7.4: php-7.4-cli php-7.4-fpm php-7.4-swoole

php-7.4-cli:
	 docker build --pull --build-arg base_image=7.4-cli --build-arg extensions="pdo pdo_mysql zip intl gd xdebug" --tag ${REGISTRY}:7.4-cli .

php-7.4-fpm:
	 docker build --pull --build-arg base_image=7.4-fpm  --build-arg extensions="pdo pdo_mysql zip intl gd" --tag ${REGISTRY}:7.4-fpm .

php-7.4-swoole:
	docker build --pull --build-arg base_image=7.4-cli --build-arg extensions="pdo pdo_mysql zip intl gd sockets swoole inotify" --tag ${REGISTRY}:swoole --tag ${REGISTRY}:7.4-swoole .

php-8.0: php-8.0-cli php-8.0-fpm php-8.0-swoole

php-8.0-cli:
	 docker build --pull --build-arg base_image=8.0-cli --build-arg extensions="pdo pdo_mysql zip intl gd xdebug" --tag ${REGISTRY}:8.0-cli .

php-8.0-fpm:
	 docker build --pull --build-arg base_image=8.0-fpm  --build-arg extensions="pdo pdo_mysql zip intl gd" --tag ${REGISTRY}:8.0-fpm .

php-8.0-swoole:
	docker build --pull --build-arg base_image=8.0-cli --build-arg extensions="pdo pdo_mysql zip intl gd sockets swoole inotify" --tag ${REGISTRY}:8.0-swoole .

